//
//  MVPLoginTests.swift
//  MVPComputerConfigurator
//
//  Created by Chirone on 19/05/2017.
//  Copyright © 2017 Chirone. All rights reserved.
//

import XCTest
import UIKit

@testable import MCVE


class MockAPIManager: LoginAPIProtocol{
	var accessToken: String?
	var refreshToken: String?
	
	func getAuthTokenWith(username: String, password: String, completion: @escaping (_: (request: URLRequest?, result: Any?, error: Error?)) -> Void) {
		
		DispatchQueue.main.asyncAfter(deadline: .now() + 2) {
			if username == "user" && password == "password" {
				let result = ["access_token": "0987654321",
				              "refresh_token": "1234567890"]
				completion((request: nil, result: result, error: nil))
			} else {
				let error = NSError(domain: "test", code: 403, userInfo: nil)
				completion((request: nil, result: nil, error: error))
			}
		}
	}
}


class MVPLoginTests: XCTestCase {
	
    override func setUp() {
        super.setUp()
        // Put setup code here. This method is called before the invocation of each test method in the class.
    }
    
    override func tearDown() {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
        super.tearDown()
    }
    
	func testLoginEmptyFieldFailure() {
		let viewModel = LoginViewModel()
		let loginAPI = MockAPIManager()
		viewModel.loginAPI = loginAPI
		
		let expect = expectation(description: "testing for empty fields")
		
		viewModel.loginWith(username: "", password: "", completion: { loginCompletion in
			
			do {
				try loginCompletion()
				XCTFail("Empty Login didn't error")
				expect.fulfill()
			} catch let error {
				XCTAssertEqual(error as? LoginError, LoginError.emptyFields)
				expect.fulfill()
			}
			
		})
		
		waitForExpectations(timeout: 10) { error in
			XCTAssertNil(error)
		}
	}
	
	func testLoginWrongUsernameOrPasswordFailure() {
		let viewModel = LoginViewModel()
		let loginAPI = MockAPIManager()
		viewModel.loginAPI = loginAPI
		
		let expect = expectation(description: "testing for incorrect credentials")
		
		viewModel.loginWith(username: "qwerty", password: "qwerty", completion: { loginCompletion in
			
			do {
				try loginCompletion()
				XCTFail("Wrong Login didn't error")
				expect.fulfill()
			} catch let error {
				XCTAssertEqual(error as? LoginError, LoginError.wrongCredentials)
				expect.fulfill()
			}
		})
		
		waitForExpectations(timeout: 10) { error in
			XCTAssertNil(error)
		}
	}
	
	func testLoginCorrectCredentials() {
		let viewModel = LoginViewModel()
		let loginAPI = MockAPIManager()
		viewModel.loginAPI = loginAPI
		let expect = expectation(description: "Testing for correct credentials")
		
		viewModel.loginWith(username: "user", password: "password", completion: { loginCompletion in
			do {
				try loginCompletion()
				expect.fulfill()
			} catch {
				XCTFail("Login didn't succeed")
				expect.fulfill()
			}
		})
		
		waitForExpectations(timeout: 10) { error in
			XCTAssertNil(error)
		}
	}
		
}
