//
//  LoginViewModel.swift
//  MVPComputerConfigurator
//
//  Created by Chirone on 18/05/2017.
//  Copyright © 2017 Chirone. All rights reserved.
//

import UIKit


protocol LoginAPIProtocol {
	var accessToken: String? { get set }
	var refreshToken: String? { get set }
	
	func getAuthTokenWith(username: String, password: String, completion: @escaping (_: (request: URLRequest?, result: Any?, error: Error?)) -> Void)
}

class LoginViewModel {
	var loginAPI: LoginAPIProtocol?
	
	func loginWith(username: String, password: String, completion: @escaping (_: () throws -> Void ) -> Void) {
		
		if username.characters.count == 0 || password.characters.count == 0 {
			completion( { throw LoginError.emptyFields } )
		}
		guard var loginAPI = loginAPI else { return }
		
		loginAPI.getAuthTokenWith(username: username, password: password, completion: { request, result, error in
			if let error = error {
				
				let errorCode = error.code
				if errorCode == 403 {
					completion({ throw LoginError.wrongCredentials })
				} else {
					completion({ throw error })
				}
				
				return
			}
			
			let responseJSON = result as! [String: AnyObject]
			guard
				let refreshToken = responseJSON["refresh_token"] as? String,
				let accessToken = responseJSON["access_token"] as? String else {
					completion({ throw LoginError.noAccessToken })
					return;
			}
			//--- set the tokens
			loginAPI.accessToken = accessToken
			loginAPI.refreshToken = refreshToken
			completion({ })
		})
	}
	
}
